package com.softtek.academy.javaweb.todo.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.todo.model.ToDoListModel;
import com.softtek.academy.javaweb.todo.service.Connectiondb;

public class ToDoList {

	public static List<ToDoListModel> getToDoList() {
		ToDoListModel listtodo;
		List<ToDoListModel> listToDo = new ArrayList<ToDoListModel>();
		try {
			 Connection conn = Connectiondb.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = 0");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				listtodo = new ToDoListModel();
				listtodo.setActivity(rs.getString("list"));
				listtodo.setId(rs.getInt("id"));
				listtodo.setIsdone(rs.getBoolean("is_done"));
				listToDo.add(listtodo);
			}
			
		}catch (Exception e) {
			System.out.println(e);
			return listToDo;
		}
		return listToDo;
		
	}
	
	public static List<ToDoListModel> getDoneList() {
		ToDoListModel listtodo;
		List<ToDoListModel> listToDo = new ArrayList();
		try {
			 Connection conn = Connectiondb.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = 1");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				listtodo = new ToDoListModel();
				listtodo.setActivity(rs.getString("list"));
				listtodo.setId(rs.getInt("id"));
				listtodo.setIsdone(rs.getBoolean("is_done"));
				listToDo.add(listtodo);
			}
			
		}catch (Exception e) {
			System.out.println(e);
			return listToDo;
		}
		return listToDo;
		
	}
	
	public static void setUpdate (String activity) {
		try {
			Connection conn = Connectiondb.getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE TO_DO_LIST SET is_done = 1 WHERE id=?");
			ps.setString(1, activity);
			ps.executeUpdate();
		}catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public static void setToDo (String list) {
		try {
			System.out.println("TODOJAVA");
			System.out.println(list);
		Connection conn = Connectiondb.getConnection();
		PreparedStatement ps = conn.prepareStatement("INSERT INTO TO_DO_LIST (list) values(?) ");
		ps.setString(1, list);
		ps.executeUpdate();
		PreparedStatement ps1 = conn.prepareStatement("select * from TO_DO_LIST");
		ResultSet rs = ps1.executeQuery();
		while(rs.next()) {
			System.out.print(rs.getString("list"));
		}
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}
