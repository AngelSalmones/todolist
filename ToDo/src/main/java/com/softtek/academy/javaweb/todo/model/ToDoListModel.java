package com.softtek.academy.javaweb.todo.model;

import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.todo.dao.ToDoList;

public class ToDoListModel {
	String activity ;
	boolean isdone;
	int id;
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public boolean isIsdone() {
		return isdone;
	}
	public void setIsdone(boolean isdone) {
		this.isdone = isdone;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void inset (String activity) {
		ToDoList.setToDo(activity);
	}
	public void getall () {
		System.out.println(ToDoList.getToDoList());
	}
	public void update (String activity) {
		ToDoList.setUpdate(activity);
	}
	
	public static List<ToDoListModel> gettodolist (){
		return ToDoList.getToDoList();
		
		
	}
	public static List<ToDoListModel> getdonelist (){
		
		return ToDoList.getDoneList();
	}
	
	@Override
	public String toString() {
		return this.activity;
	}
	
	public String id() {
		return (String.valueOf(this.id));
	}
}
