<%
	String contextPath = request.getContextPath();
	String title = "To Do List";
	%>
<html>
<head>
<title><%= title%></title>
</head>
<body>
<h2>To Do List</h2>

<ul>
	<li>------------------------------------------</li>
	
	
	<li><a href = "<%= contextPath%>/views/newActivity.jsp">Add a New Activity</a></li>
	<li><a href = "<%= contextPath%>/views/toDoList.jsp">Go to My To Do List</a></li>
</ul>
</body>
</html>